# eu_cookie_compliance private files consent storage

## Description
eu_cookie_compliance ConsentStorage Plugin that writes cookie consent to log 
files in private://consentlogs.

## Dependencies 
* [EU Cookie Compliance](https://www.drupal.org/project/eu_cookie_compliance)

## Installation
Install the Editor File upload module as you would normally install a 
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.

After installation, head over to the EU Cookie Compliance settings > Store 
record of consent, select `Private files storage`, and save.

Go to the Private files Consent storage tab, and choose the preferred interval 
for the log file creation (Daily, Weekly, and Monthly).

### File naming scheme
- **Daily:** eucc_consent_YYYY-MM-DD.log
- **Weekly:** eucc_consent_YYYY-wXX.log (XX being the week number)
- **Monthly:** eucc_consent_YYYY-MM.log

## Credits
This module is maintained by [openup.media](https://www.drupal.org/open-up-media).
