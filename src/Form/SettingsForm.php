<?php

namespace Drupal\eucc_consent_file\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure eucc_consent_file settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eucc_consent_file_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['eucc_consent_file.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $defaultStorageMethod = 'monthly';
    if (!empty($this->config('eucc_consent_file.settings')->get('storage_method'))) {
      $defaultStorageMethod = $this->config('eucc_consent_file.settings')->get('storage_method');
    }

    $form['storage_method'] = [
      '#type' => 'radios',
      '#title' => t('Please select the interval'),
      '#description' => t('This will create a new log on a daily, weekly, or monthly basis.') . '<br /><br /><strong>' . t('File naming scheme') . ':</strong><br />' . t('Daily') . ': eucc_consent_YYYY-MM-DD.log<br />' . t('Weekly') . ': eucc_consent_YYYY-wXX.log<br />' . t('Monthly') . ': eucc_consent_YYYY-MM.log',
      '#default_value' => $defaultStorageMethod,
      '#options' => [
        'daily' => t('Daily'),
        'weekly' => t('Weekly'),
        'monthly' => t('Monthly'),
      ],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('eucc_consent_file.settings')
      ->set('storage_method', $form_state->getValue('storage_method'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
