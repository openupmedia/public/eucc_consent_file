<?php

namespace Drupal\eucc_consent_file\Plugin\ConsentStorage;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\eu_cookie_compliance\Plugin\ConsentStorageBase;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a file storage for cookie consents.
 *
 * @ConsentStorage(
 *   id = "file",
 *   name = @Translation("Private files storage"),
 *   description = @Translation("Local private files storage")
 * )
 */
class FileConsentStorage extends ConsentStorageBase {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * BasicConsentStorage constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory);

    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function registerConsent($consent_type) {
    $revision_id = $this->getCurrentPolicyNodeRevision();
    $timestamp = $this->time->getRequestTime();
    $ip_address = \Drupal::request()->getClientIp();
    $uid = \Drupal::currentUser()->id();

    $defaultStorageMethod = 'monthly';
    if (!empty(\Drupal::config('eucc_consent_file.settings')->get('storage_method'))) {
      $defaultStorageMethod = \Drupal::config('eucc_consent_file.settings')->get('storage_method');
    }

    $file = "eucc_consent.log";
    switch ($defaultStorageMethod) {
      case 'daily':
        $file = "eucc_consent_" . date('Y-m-d', $timestamp) . ".log";
        break;

      case 'weekly':
        $file = "eucc_consent_" . date('Y', $timestamp) . "-w" . date('W', $timestamp) . ".log";
        break;

      case 'monthly':
      default:
        $file = "eucc_consent_" . date('Y-m', $timestamp) . ".log";
        break;
    }

    $dir = \Drupal::service('file_system')->realpath("private://") . "/consentlogs/";
    if (!is_dir($dir)) {
      if (!mkdir($dir) && !is_dir($dir)) {
        throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
      }
    }
    $file = $dir . $file;
    $data = [
      'uid' => $uid,
      'ip_address' => $ip_address,
      'timestamp' => $timestamp,
      'revision_id' => $revision_id ?: 0,
      'consent_type' => $consent_type,
    ];
    file_put_contents($file, json_encode($data) . PHP_EOL, FILE_APPEND);

    return TRUE;
  }

}
